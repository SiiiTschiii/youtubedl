module gitlab.ethz.ch/chgerber/youtubedl

go 1.12

require (
	github.com/asticode/go-astisub v0.0.0-20190514140258-c0ed7925c393
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.3.0
	gitlab.ethz.ch/chgerber/MessageComposition v0.0.0-20190606100759-1b591f45e7e5
	gitlab.ethz.ch/chgerber/annotation/v2 v2.2.0
	gitlab.ethz.ch/chgerber/monitor v0.0.0-20190527191251-2bb9dd731340
	go.mongodb.org/mongo-driver v1.0.3
	google.golang.org/api v0.6.0
)
