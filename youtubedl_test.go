package youtubedl

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.ethz.ch/chgerber/annotation/v2"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"testing"
)

func TestMain(m *testing.M) {

	// Make sure youtube-dl binary exists
	_, err := exec.LookPath("youtube-dl")
	if err != nil {
		log.Fatal(err)
	}
	cmd := exec.Command("youtube-dl", "--version")
	_, err = cmd.Output()
	if err != nil {
		log.Fatal(err)
	}
	os.Exit(m.Run())
}

func TestGetPlaylistVideos(t *testing.T) {
	assert.Equal(t, true, os.Getenv("YOUTUBE_DATA_API_KEY") != "")
	videos, err := GetPlaylistVideos("PLpTJzCmGYaZFjE7jfH1tKFZt9rtqvvuj0", os.Getenv("YOUTUBE_DATA_API_KEY"))
	assert.Equal(t, nil, err)
	assert.Equal(t, 3, len(videos))
}

func TestGetYouTubeDownloadLink(t *testing.T) {
	video, audio, err := GetVideoURL("OsFEV35tWsg")
	assert.Equal(t, nil, err)
	assert.Equal(t, "/videoplayback", video.Path)
	assert.Equal(t, "/videoplayback", audio.Path)
}

func TestVideoIDToSub(t *testing.T) {

	info, err := getVideoInfo("OsFEV35tWsg")
	assert.Equal(t, nil, err)
	if tags, ok := info["tags"].([]interface{}); ok {
		if tag, ok := tags[0].(string); ok {
			assert.Equal(t, "TED", tag)
		} else {
			t.Errorf("got data of type %T but wanted string", tag)
		}
	} else {
		t.Errorf("got data of type %T but wanted []interface{}", info["tags"])
	}
}

func TestListSubs(t *testing.T) {

	subs, err := listSubs("OsFEV35tWsg")
	assert.Equal(t, nil, err)
	assert.Equal(t, "vtt", subs.AutomaticCaptions["en"][0])
	assert.Equal(t, "ttml", subs.AutomaticCaptions["en"][1])
	assert.Equal(t, "vtt", subs.Subtitles["en"][0])
	assert.Equal(t, "ttml", subs.Subtitles["en"][1])
	assert.Equal(t, true, len(subs.Subtitles) > 0)
	assert.Equal(t, true, len(subs.AutomaticCaptions) > 0)

}

func TestFindSub(t *testing.T) {
	formats, err := findSub("OsFEV35tWsg", "en")
	assert.Equal(t, nil, err)
	assert.Equal(t, true, len(formats) > 0)
	assert.Equal(t, "vtt", formats[0])
	assert.Equal(t, "ttml", formats[1])
}

func TestSubDownload(t *testing.T) {

	id := "OsFEV35tWsg"
	language := "en"
	format := "vtt"
	filename := filepath.Join("/tmp", "sub-"+id)
	err := downloadSubtitle(false, id, language, format, filename)
	assert.Equal(t, nil, err)
	fileWithSuffix := filename + "." + language + "." + format
	_, err = os.Stat(fileWithSuffix)
	assert.Equal(t, nil, err)
	assert.Equal(t, false, os.IsNotExist(err))
	assert.Equal(t, nil, os.Remove(fileWithSuffix))
}

func TestSubDownloadLanguageNotSupported(t *testing.T) {

	id := "OsFEV35tWsg"
	language := "zz"
	format := "vtt"
	filename := filepath.Join("/tmp", "sub-"+id)
	err := downloadSubtitle(false, id, language, format, filename)
	assert.Equal(t, fmt.Sprintf("subtitles language %s not available", language), err.Error())
}

func TestSubDownloadFormatNotSupported(t *testing.T) {

	id := "OsFEV35tWsg"
	language := "en"
	format := "srt"
	filename := filepath.Join("/tmp", "sub-"+id)
	err := downloadSubtitle(false, id, language, format, filename)
	assert.Equal(t, fmt.Sprintf("subtitle format %s not found", format), err.Error())
}

func TestAutomaticCaptionDownload(t *testing.T) {

	id := "OsFEV35tWsg"
	language := "en"
	format := "vtt"
	filename := filepath.Join("/tmp", "automaticCaption-"+id)
	err := downloadSubtitle(true, id, language, format, filename)
	assert.Equal(t, nil, err)
	fileWithSuffix := filename + "." + language + "." + format
	_, err = os.Stat(fileWithSuffix)
	assert.Equal(t, nil, err)
	assert.Equal(t, false, os.IsNotExist(err))
	assert.Equal(t, nil, os.Remove(fileWithSuffix))
}

func TestGetSubtitle(t *testing.T) {
	sub, err := GetSubtitle(false, "OsFEV35tWsg", "en")
	assert.Equal(t, nil, err)
	assert.Equal(t, "Philosophers, dramatists, theologians", sub.Annotations("", annotation.Playlist{ID: "1", Name: "TED: The psychology of evil"})[0].Subtitle.Text)
}

func TestUnavailableVideo(t *testing.T) {
	_, err := listSubs("vBc5vR5c_VE")
	assert.IsType(t, ErrYoutubeVideoUnavailable(""), err)
}

func TestYouTubeSRV3Loader(t *testing.T) {
	xmlFile, err := os.Open("testData/testSubYouTube-VIlLpnJJl_4.en.srv3")
	assert.Equal(t, nil, err)

	// defer the closing of our xmlFile so that we can parse it later on
	defer xmlFile.Close()

	// read our opened xmlFile as a byte array.
	byteValue, err := ioutil.ReadAll(xmlFile)
	assert.Equal(t, nil, err)

	timedText, err := parseSRV3(byteValue)
	assert.Equal(t, nil, err)
	assert.Equal(t, 170, timedText.Body.Paragraphs[0].Time)
	assert.Equal(t, 7810, timedText.Body.Paragraphs[0].Duration)
	assert.Equal(t, "you're", timedText.Body.Paragraphs[0].Segments[0].Value)
	assert.Equal(t, 0, timedText.Body.Paragraphs[2].Segments[0].Time)

	assert.Equal(t, 1571, timedText.Body.Paragraphs[2].Segments[3].Time)
	assert.Equal(t, 226, timedText.Body.Paragraphs[2].Segments[3].Duration)
	assert.Equal(t, "12", timedText.Body.Paragraphs[2].Segments[3].Value)

}

func TestTimedTextToAnnotationAutomaticCaption(t *testing.T) {
	xmlFile, err := os.Open("testData/testSubYouTube-VIlLpnJJl_4.en.srv3")
	assert.Equal(t, nil, err)
	// defer the closing of our xmlFile so that we can parse it later on
	defer xmlFile.Close()
	// read our opened xmlFile as a byte array.
	byteValue, err := ioutil.ReadAll(xmlFile)
	assert.Equal(t, nil, err)
	timedText, err := parseSRV3(byteValue)
	assert.Equal(t, nil, err)

	a := timedText.Annotations("youtube://VIlLpnJJl_4", annotation.Playlist{ID: "VIlLpnJJl_4", Name: "Ninja: Stories Of A Stream Sniper!"})

	aExpect := annotation.Annotation{
		Src: "youtube://VIlLpnJJl_4",
		Subtitle: annotation.Subtitle{
			Count: 1,
			Text:  "you're back in my day we use differently",
			Start: 170,
			End:   4539,
			Segments: []annotation.TimedText{
				{Start: 170, End: 1429, Text: "you're"},
				{Start: 1429, End: 2429, Text: "back"},
				{Start: 2429, End: 2460, Text: "in"},
				{Start: 2460, End: 2909, Text: "my"},
				{Start: 2909, End: 2970, Text: "day"},
				{Start: 2970, End: 3510, Text: "we"},
				{Start: 3510, End: 3720, Text: "use"},
				{Start: 3720, End: 4539, Text: "differently"},
			},
		},
		Playlist: annotation.Playlist{ID: "VIlLpnJJl_4", Name: "Ninja: Stories Of A Stream Sniper!"},
	}
	assert.Equal(t, aExpect, *a[0])

	aExpect = annotation.Annotation{
		Src: "youtube://VIlLpnJJl_4",
		Subtitle: annotation.Subtitle{
			Count: 16,
			Text:  "hundreds of thousands and millions of",
			Start: 59760,
			End:   60889,
			Segments: []annotation.TimedText{
				{Start: 59760, End: 60090, Text: "hundreds"},
				{Start: 60090, End: 60149, Text: "of"},
				{Start: 60149, End: 60510, Text: "thousands"},
				{Start: 60510, End: 60629, Text: "and"},
				{Start: 60629, End: 60690, Text: "millions"},
				{Start: 60690, End: 60889, Text: "of"},
			},
		},
		Playlist: annotation.Playlist{ID: "VIlLpnJJl_4", Name: "Ninja: Stories Of A Stream Sniper!"},
	}
	assert.Equal(t, aExpect, *a[15])
}

func TestTimedTextToAnnotations(t *testing.T) {

	snipped := `
	<?xml version="1.0" encoding="utf-8" ?>
	<timedtext format="3">
		<head>
			<pen id="1" fc="#E5E5E5"/>
			<pen id="2" fc="#CCCCCC"/>
			<ws id="0"/>
			<ws id="1" mh="2" ju="0" sd="3"/>
			<wp id="0"/>
			<wp id="1" ap="6" ah="20" av="100" rc="2" cc="40"/>
		</head>
		<body>
			<w t="0" id="1" wp="1" ws="1"/>
			<p t="170" d="7810" w="1">
				<s p="1" ac="202">you&#39;re</s>
				<s t="1259" ac="249">back</s>
				<s t="2259" ac="252">in</s>
				<s p="2" t="2290" ac="135">my</s>
				<s t="2739" ac="252">day</s>
				<s t="2800" ac="252">we</s>
				<s t="3340" ac="252">use</s>
				<s p="2" t="3550" ac="0">differently</s>
			</p>
			<p t="4549" d="5681" w="1">
				<s p="2" ac="144">stream</s>
				<s p="2" t="1000" ac="94">snape</s>
				<s p="1" t="1151" ac="200">for</s>
				<s t="1571" ac="226">12</s>
				<s t="1871" ac="252">hours</s>
				<s p="2" t="2111" ac="161">a</s>
				<s t="2381" ac="252">day</s>
				<s t="2591" ac="252">just</s>
				<s p="2" t="2830" ac="187">to</s>
        	</p>
			<p t="12794" d="3556">Philosophers, dramatists, theologians</p>
			<p t="16374" d="2206">have grappled with this question for centuries:</p>
			<p t="18604" d="1532">what makes people go wrong?</p>
			<p t="20160" d="2976">Interestingly, I asked this question when I was a little kid.</p>
			<p t="75530" d="1450"> (Laughter) </p>
			<p t="77250" d="4140"> Or maybe you were thinking &quot;Where do you get her confidence?&quot; </p>
		</body>
	</timedtext>
    `

	// read our opened xmlFile as a byte array.
	timedText, err := parseSRV3([]byte(snipped))
	assert.Equal(t, nil, err)

	// Test for the right number of annotations
	annotations := fromTimedText(timedText, "", annotation.Playlist{})
	assert.Equal(t, 8, len(annotations))
	assert.Equal(t, 8, len(annotations[0].Subtitle.Segments))
	assert.Equal(t, 9, len(annotations[1].Subtitle.Segments))
	assert.Equal(t, 0, len(annotations[2].Subtitle.Segments))
	assert.Equal(t, 0, len(annotations[3].Subtitle.Segments))

	// Test if segment text matches
	for i, a := range annotations {

		// TODO test also text of paragraphs with value but zero segments
		for segmentIdx, segment := range a.Subtitle.Segments {
			assert.Equal(t, timedText.Body.Paragraphs[i].Segments[segmentIdx].Value, segment.Text)
		}

		if len(a.Subtitle.Segments) == 0 {
			assert.Equal(t, timedText.Body.Paragraphs[i].Value, a.Subtitle.Text)
		}
	}

	aExpect := annotation.Annotation{
		Src: "",
		Subtitle: annotation.Subtitle{
			Count: 8,
			Text:  " Or maybe you were thinking \"Where do you get her confidence?\" ",
			Start: 77250,
			End:   81390,
		},
	}
	assert.Equal(t, aExpect, *annotations[7])
}

func TestSRV3parseManualCaption(t *testing.T) {
	xmlFile, err := os.Open("testData/subManual-OsFEV35tWsg.en.srv3")
	assert.Equal(t, nil, err)

	// defer the closing of our xmlFile so that we can parse it later on
	defer xmlFile.Close()

	// read our opened xmlFile as a byte array.
	byteValue, err := ioutil.ReadAll(xmlFile)
	assert.Equal(t, nil, err)

	timedText, err := parseSRV3(byteValue)
	assert.Equal(t, nil, err)
	assert.Equal(t, 12794, timedText.Body.Paragraphs[0].Time)
	assert.Equal(t, 3556, timedText.Body.Paragraphs[0].Duration)
	assert.Equal(t, "Philosophers, dramatists, theologians", timedText.Body.Paragraphs[0].Value)

	assert.Equal(t, 18604, timedText.Body.Paragraphs[2].Time)
	assert.Equal(t, 1532, timedText.Body.Paragraphs[2].Duration)
	assert.Equal(t, "what makes people go wrong?", timedText.Body.Paragraphs[2].Value)
}
