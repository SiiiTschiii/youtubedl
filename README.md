# youtubedl
[![pipeline status](https://gitlab.ethz.ch/chgerber/youtubedl/badges/master/pipeline.svg)](https://gitlab.ethz.ch/chgerber/youtubedl/commits/master)
[![coverage report](https://gitlab.ethz.ch/chgerber/youtubedl/badges/master/coverage.svg)](https://gitlab.ethz.ch/chgerber/youtubedl/commits/master)

Go wrapper library for youtube-dl and YouTube Data API

### Requirements
* Env. variable `YOUTUBE_DATA_API_KEY` needs to be set.
* Binary `youtube-dl` needs to be callable.
* `youtube-dl` requires `python`

### youtube-dl linux cli tool

Upon Errors make sure the most recent version is installed:
```bash
sudo -H pip install --upgrade youtube-dl
```

Download video
```bash
youtube-dl https://youtu.be/1yRFJcCg6xo

```

Download video info
```bash
youtube-dl --skip-download --write-info-json https://youtu.be/1yRFJcCg6xo

# pretty print in command line
youtube-dl --skip-download -j https://youtu.be/1yRFJcCg6xo | jq

```

Download video & thumnail
```bash
youtube-dl --skip-download --write-thumbnail https://youtu.be/1yRFJcCg6xo
```

Download auto generated english subs of type vtt 
```bash
youtube-dl --skip-download --write-auto-sub --sub-format vtt --sub-lang en https://youtu.be/1yRFJcCg6xo
```

Dump Playlist info as list of json (stdout)
```bash
youtube-dl -j --flat-playlist https://www.youtube.com/playlist\?list\=PLuXXbBFpPc0lb4_FdI1NOPTCAYroXOywl
```

Download Playlist
```bash
youtube-dl https://www.youtube.com/playlist\?list\=PLuXXbBFpPc0lb4_FdI1NOPTCAYroXOywl
# Leads to error: youtube_dl.utils.RegexNotFoundError: Unable to extract Initial JS player signature function name

```

Get URL of video and audio
````bahs
youtube-dl -g https://youtu.be/1yRFJcCg6xo
````

Download a fragment of a youtube video
```bash
ffmpeg $(youtube-dl -g https://www.youtube.com/watch\?v\=HXGwJevjOfs | sed "s/.*/-ss 10 -i &/") -t 60 -c copy test.mkv
```